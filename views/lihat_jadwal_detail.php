<?php
    include "models/m_pegawai.php";
    include "models/m_jadwal.php";
    $pgw = new Pegawai($connection);
    $jwl = new Jadwal($connection);
?>
<!-- ============================================================== -->
<!-- Start Page Content -->
<!-- ============================================================== -->
<div class="row">
    <!-- column -->
    <div class="col-lg-12">
        <div class="card">
            <div class="card-block">
            <h4 class="card-title">Informasi Data Acara</h4>
            <hr>
                <?php
                    $NAMA_ACARA = '';
                    $PRODUSER_NIP = '';
                    $TANGGAL_MULAI = '';
                    $TANGGAL_SELESAI = '';
                    $LOKASI = '';
                    $JAM = '';
                    $BUKTI_TUGAS = '';
                    if (isset($_GET['detail'])) {
                        $acara = $jwl->tampil($_GET['detail']);
                        $acara = $acara->fetch_object();
                        $NAMA_ACARA = $acara->NAMA_ACARA;
                        $PRODUSER_NIP = $acara->NAMA.' - '.$acara->PRODUSER_NIP;
                        $TANGGAL_MULAI = $acara->TANGGAL_MULAI;
                        $TANGGAL_SELESAI = $acara->TANGGAL_SELESAI;
                        $LOKASI = strtoupper($acara->LOKASI);
                        $JAM = $acara->JAM;
                        $BUKTI_TUGAS = $acara->BUKTI_TUGAS;
                    }
                ?>
                <form acion="" method="post" class="form-material">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="nama-acara" class="col-md-12">NAMA ACARA</label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control form-control-line" name="NAMA_ACARA" id="NAMA_ACARA" value="<?php echo $NAMA_ACARA;?>" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for ="nama-produser"class="col-md-12">Pilih Produser Acara</label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control form-control-line" name="PRODUSER_NIP" id="PRODUSER_NIP" value="<?php echo $PRODUSER_NIP;?>" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="tanggal-mulai" class="col-md-12">Tanggal Mulai</label>
                                <div class="col-md-12">
                                    <input type="date" class="form-control form-control-line"name="TANGGAL_MULAI" id="TANGGAL_MULAI" value="<?php echo $TANGGAL_MULAI;?>" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="tanggal-mulai" class="col-md-12">Tanggal Selesai</label>
                                <div class="col-md-12">
                                    <input type="date" class="form-control form-control-line"name="TANGGAL_SELESAI" id="TANGGAL_SELESAI" value="<?php echo $TANGGAL_SELESAI;?>" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for ="nama-produser"class="col-md-12">Jam Acara</label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control form-control-line" name="JAM" id="JAM" value="<?php echo $JAM;?>" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="tanggal-mulai" class="col-md-12">Lokasi Acara</label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control form-control-line" name="LOKASI" id="LOKASI" value="<?php echo $LOKASI;?>" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="tanggal-mulai" class="col-md-12">Bukti Tugas Dari Produser</label>
                                <div class="col-md-12">
                                <?php
                                    if ($BUKTI_TUGAS != NULL || $BUKTI_TUGAS != '') {
                                        ?>
                                            <a href="<?php echo $BUKTI_TUGAS;?>" target="_blank" class="btn waves-effect waves-light btn-success btn-xs">UNDUH BUKTI TUGAS</a>
                                        <?php
                                    } else {
                                        ?>
                                            <button class="btn waves-effect waves-light btn-danger btn-xs" disabled>BELUM MENGUNGGAH</button>
                                        <?php
                                    }
                                ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <br>
                            <h4 class="card-title">Daftar Penugasan & Bukti Hasil Tugas Pegawai</h4>
                            <hr>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <?php
                                    $jabatan = $pgw->query('SELECT JABATAN FROM tbl_pegawai WHERE JABATAN != "PRODUSER" GROUP BY JABATAN ORDER BY JABATAN ASC');
                                    if (!$jabatan || @$jabatan->num_rows == 0) {
                                        echo '<option>Tidak Ada Pegawai Selain Produser</option>';
                                    } else {
                                        $no=0;
                                        while ($row = $jabatan->fetch_object()) {
                                            $sql = 'SELECT * FROM tbl_pegawai t, tbl_penugasan p  WHERE t.JABATAN = "'.$row->JABATAN.'" AND p.KODE_JADWAL = "'.$_GET['detail'].'" AND p.NIP = t.NIP ORDER BY t.NAMA ASC';
                                            $pegawai = $pgw->query($sql);
                                            if (!$pegawai || @$pegawai->num_rows == 0) {
                                                ?>
                                                    <label class="col-12 col-form-label"><strong><?php echo strtoupper($row->JABATAN);?></strong></label>
                                                    <div class="input-group">
                                                        <div class="col-md-12">
                                                            <option>Tidak Ada</option>
                                                        <input type="hidden" name="TOTAL_PEGAWAI" value="<?php echo $no;?>"/>
                                                        </div>
                                                    </div>
                                                    <hr>
                                                <?php
                                            } else {
                                                ?>
                                                    <label class="col-12 col-form-label"><strong><?php echo strtoupper($row->JABATAN);?></strong></label>
                                                    <div class="input-group">
                                                        <div class="col-md-12">
                                                <?php
                                                while ($data = $pegawai->fetch_object()) {
                                                    ?>
                                                        <div class="col-md-6" style="float: left;">
                                                            <div class="input-group-text">
                                                                <input type="checkbox" name="pegawai[<?php echo $no;?>]" id="pegawai[<?php echo $no;?>]" value="<?php echo $data->NIP;?>" class="filled-in chk-col-cyan" disabled>
                                                                <label for="pegawai[<?php echo $no;?>]" class="mb-0"><?php echo ucfirst($data->NAMA);?></label>
                                                                <?php
                                                                    if ($_SESSION['login_as'] == 'atasan') {
                                                                        if ($data->BUKTI_TUGAS != NULL) {
                                                                            ?>
                                                                                <a href="<?php echo $data->BUKTI_TUGAS;?>" target="_blank" class="btn waves-effect waves-light btn-success btn-xs">UNDUH BUKTI</a>
                                                                            <?php
                                                                        } else {
                                                                            ?>
                                                                                <button class="btn waves-effect waves-light btn-danger btn-xs" disabled>BELUM MENGUNGGAH</button>
                                                                            <?php
                                                                        }
                                                                    }
                                                                ?>
                                                            </div>
                                                        </div>
                                                    <?php
                                                    $no++;
                                                }
                                                ?>
                                                        <input type="hidden" name="TOTAL_PEGAWAI" value="<?php echo $no;?>"/>
                                                        </div>
                                                    </div>
                                                    <hr>
                                                <?php
                                            }        
                                        }
                                    }
                                ?>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <hr>
                        </div>
                        <!-- <div class="form-group">
                            <div class="col-sm-12">
                                <input type="submit" class="btn btn-success" name="tambah" value="BUAT JADWAL"/>
                            </div>
                        </div> -->
                    </div>
                </form>
            </div>
        </div>
    </div>        
</div>
<!-- ============================================================== -->
<!-- End PAge Content -->
<!-- ============================================================== -->
